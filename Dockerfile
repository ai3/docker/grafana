FROM debian:stable-slim

COPY grafana.gpg /usr/share/keyrings/grafana.gpg
COPY build.sh /tmp/build.sh

RUN /tmp/build.sh && rm /tmp/build.sh

WORKDIR /usr/share/grafana
ENTRYPOINT ["/usr/sbin/grafana-server", "--config=/etc/grafana/grafana.ini", "--packaging=deb", "cfg:default.paths.logs=/var/log/grafana", "cfg:default.paths.data=/var/lib/grafana", "cfg:default.paths.plugins=/var/lib/grafana/plugins", "cfg:default.paths.provisioning=/etc/grafana/provisioning"]

