#!/bin/sh
#
# Install script for Grafana inside a Docker container
# (using Debian packages).
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="ca-certificates apt-transport-https"

# Packages to install.
PACKAGES="
        grafana
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e

apt-get update

install_packages ${BUILD_PACKAGES}

# Install the Grafana repository.
echo "deb [signed-by=/usr/share/keyrings/grafana.gpg] https://apt.grafana.com stable main" \
    > /etc/apt/sources.list.d/grafana.list
apt-get -q update

install_packages ${PACKAGES}

# For some reason the Debian package installs the /etc/default file
# with very restrictive permissions.
chmod 644 /etc/default/grafana-server

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
